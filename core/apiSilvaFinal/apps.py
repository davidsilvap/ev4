from django.apps import AppConfig


class ApisilvafinalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apiSilvaFinal'
