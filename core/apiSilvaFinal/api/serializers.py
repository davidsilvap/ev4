from rest_framework.serializers import ModelSerializer
from apiSilvaFinal.models import informatica, telecomunicaciones, ciberseguridad

class PostSerilizer(ModelSerializer):
    class Meta:
        model = informatica
        fields = ['id','nombreCarrera','cantidadAnios','cantidadAsignaturas','capacidadAlumnos']