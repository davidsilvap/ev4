from django.views import View
from .models import informatica, telecomunicaciones, ciberseguridad
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
import json

class InformaticaView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if(id > 0):
            carrInformatica = list(informatica.objects.filter(id=id).values())
            if len(carrInformatica)>0:
                informatica = carrInformatica[0]
                datos = {'message':"Succes", 'carrInformatica': informatica}
            else:
                datos = {'message':"Carrera no encontrado."}
            return JsonResponse(datos)
        else:
            carrInformatica = list(informatica.objects.values())
            if len(carrInformatica) > 0:
                datos = {'message':"Success", 'carrInformatica': carrInformatica}
            else:
                datos = {'message':"Carreras no encontradas."}
            return JsonResponse(datos)

    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        informatica.objects.create(nombreCarrera=jd['nombreCarrera'], cantidadAnios=jd['cantidadAnios'], cantidadAsignaturas=jd['cantidadAsignaturas'], capacidadAlumnos=jd['capacidadAlumnos'])
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, id):
        jd = json.loads(request.body)
        carrInformatica = list(informatica.objects.filter(id=id).values())
        if len(carrInformatica) > 0:
            informatica = informatica.objects.get(id=id)
            informatica.nombreCarrera = jd['nombreCarrera']
            informatica.cantidadAnios = jd['cantidadAnios']
            informatica.cantidadAsignaturas = jd['cantidadAsignaturas']
            informatica.capacidadAlumnos = jd['capacidadAlumnos']
            informatica.save()
            datos = {'message':"Success"}
        else:
            datos = {'message':"Carrera no encontrada."}
        return JsonResponse(datos)

    def delete(self, request, id):
        carrInformatica = list(informatica.objects.filter(id=id).values())
        if len(carrInformatica) > 0:
            informatica.objects.filter(id=id).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Carrera no encontradoa."}
        return JsonResponse(datos)

class TelecomunicacionesView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if(id > 0):
            carrTelecomunicaciones = list(telecomunicaciones.objects.filter(id=id).values())
            if len(carrTelecomunicaciones)>0:
                telecomunicaciones = carrTelecomunicaciones[0]
                datos = {'message':"Succes", 'carrTelecomunicaciones': telecomunicaciones}
            else:
                datos = {'message':"Carrera no encontrado."}
            return JsonResponse(datos)
        else:
            carrTelecomunicaciones = list(telecomunicaciones.objects.values())
            if len(carrTelecomunicaciones) > 0:
                datos = {'message':"Success", 'carrTelecomunicaciones': carrTelecomunicaciones}
            else:
                datos = {'message':"Carreras no encontradas."}
            return JsonResponse(datos)

    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        telecomunicaciones.objects.create(nombreCarrera=jd['nombreCarrera'], cantidadAnios=jd['cantidadAnios'], cantidadAsignaturas=jd['cantidadAsignaturas'], capacidadAlumnos=jd['capacidadAlumnos'])
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, id):
        jd = json.loads(request.body)
        carrTelecomunicaciones = list(telecomunicaciones.objects.filter(id=id).values())
        if len(carrTelecomunicaciones) > 0:
            telecomunicaciones = telecomunicaciones.objects.get(id=id)
            telecomunicaciones.nombreCarrera = jd['nombreCarrera']
            telecomunicaciones.cantidadAnios = jd['cantidadAnios']
            telecomunicaciones.cantidadAsignaturas = jd['cantidadAsignaturas']
            telecomunicaciones.capacidadAlumnos = jd['capacidadAlumnos']
            telecomunicaciones.save()
            datos = {'message':"Success"}
        else:
            datos = {'message':"Carrera no encontrada."}
        return JsonResponse(datos)

    def delete(self, request, id):
        carrTelecomunicaciones = list(telecomunicaciones.objects.filter(id=id).values())
        if len(carrTelecomunicaciones) > 0:
            telecomunicaciones.objects.filter(id=id).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Carrera no encontradoa."}
        return JsonResponse(datos)

class CiberseguridadView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, id=0):
        if(id > 0):
            carrCiberseguridad = list(ciberseguridad.objects.filter(id=id).values())
            if len(carrCiberseguridad)>0:
                ciberseguridad = carrCiberseguridad[0]
                datos = {'message':"Succes", 'carrCiberseguridad': ciberseguridad}
            else:
                datos = {'message':"Carrera no encontrado."}
            return JsonResponse(datos)
        else:
            carrCiberseguridad = list(ciberseguridad.objects.values())
            if len(carrCiberseguridad) > 0:
                datos = {'message':"Success", 'carrCiberseguridad': carrCiberseguridad}
            else:
                datos = {'message':"Carreras no encontradas."}
            return JsonResponse(datos)

    def post(self, request):
        # print(request.body)
        jd = json.loads(request.body)
        # print(jd)
        ciberseguridad.objects.create(nombreCarrera=jd['nombreCarrera'], cantidadAnios=jd['cantidadAnios'], cantidadAsignaturas=jd['cantidadAsignaturas'], capacidadAlumnos=jd['capacidadAlumnos'])
        datos = {'message': "Success"}
        return JsonResponse(datos)

    def put(self, request, id):
        jd = json.loads(request.body)
        carrCiberseguridad = list(ciberseguridad.objects.filter(id=id).values())
        if len(carrCiberseguridad) > 0:
            ciberseguridad = ciberseguridad.objects.get(id=id)
            ciberseguridad.nombreCarrera = jd['nombreCarrera']
            ciberseguridad.cantidadAnios = jd['cantidadAnios']
            ciberseguridad.cantidadAsignaturas = jd['cantidadAsignaturas']
            ciberseguridad.capacidadAlumnos = jd['capacidadAlumnos']
            ciberseguridad.save()
            datos = {'message':"Success"}
        else:
            datos = {'message':"Carrera no encontrada."}
        return JsonResponse(datos)

    def delete(self, request, id):
        carrCiberseguridad = list(ciberseguridad.objects.filter(id=id).values())
        if len(carrCiberseguridad) > 0:
            ciberseguridad.objects.filter(id=id).delete()
            datos = {'message': "Success"}
        else:
            datos = {'message': "Carrera no encontradoa."}
        return JsonResponse(datos)