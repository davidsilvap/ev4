from django.contrib import admin
from django.urls import path, include
from apiSilvaFinal.api.router import router_posts

urlpatterns = [
    path('admin/', admin.site.urls),
    path('apiSilvaFinal/', include(router_posts.urls)),
    path('apiSilvaFinal/informatica', include(router_posts.urls)),
    path('apiSilvaFinal/telecomunicaciones', include(router_posts.urls)),
    path('apiSilvaFinal/ciberseguridad', include(router_posts.urls)),
]
