from django.db import models

class informatica(models.Model):
    nombreCarrera = models.CharField(max_length=100)
    cantidadAnios = models.CharField(max_length=100)
    cantidadAsignaturas = models.CharField(max_length=100)
    capacidadAlumnos = models.CharField(max_length=100)

class telecomunicaciones(models.Model):
    nombreCarrera = models.CharField(max_length=100)
    cantidadAnios = models.CharField(max_length=100)
    cantidadAsignaturas = models.CharField(max_length=100)
    capacidadAlumnos = models.CharField(max_length=100)

class ciberseguridad(models.Model):
    nombreCarrera = models.CharField(max_length=100)
    cantidadAnios = models.CharField(max_length=100)
    cantidadAsignaturas = models.CharField(max_length=100)
    capacidadAlumnos = models.CharField(max_length=100)
