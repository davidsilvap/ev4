from django.urls import path
from .views import InformaticaView, TelecomunicacionesView, CiberseguridadView

urlpatterns = [
    path('informatica/', InformaticaView.as_view(), name='informatica_list'),
    path('telecomunicaciones/', TelecomunicacionesView.as_view(), name='telecomunicaciones_list'),
    path('ciberseguridad/', CiberseguridadView.as_view(), name='ciberseguridad_list'),
    path('informarica/<int:id>', InformaticaView.as_view(), name='informatica_process'),
    path('telecomunicaciones/<int:id>', TelecomunicacionesView.as_view(), name='telecomunicaciones_process'),
    path('ciberseguridad/<int:id>', CiberseguridadView.as_view(), name='ciberseguridad_process')
]