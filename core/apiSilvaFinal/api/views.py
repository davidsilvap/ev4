from rest_framework.viewsets import ModelViewSet
from apiSilvaFinal.models import informatica, telecomunicaciones, ciberseguridad
from apiSilvaFinal.api.serializers import PostSerilizer

class PostApiViewSet(ModelViewSet):
    serializer_class = PostSerilizer
    queryset = informatica.objects.all()
