from rest_framework.routers import DefaultRouter
from apiSilvaFinal.api.views import PostApiViewSet
from apiSilvaFinal.models import informatica, telecomunicaciones, ciberseguridad

router_posts = DefaultRouter(informatica)

router_posts.register(prefix='post', basename='post', viewset=PostApiViewSet)